import Creater.Creater;
import Creater.CreaterWinButton;
import Creater.CreaterHTMLButton;

public class Demo {
    public static void main(String[] args) {
        String nameEnvironment = "win";

        Creater creater = getCreater(nameEnvironment);
        creater.getButton().drawButton(); //using interface for button object

        nameEnvironment = "html";

        creater = getCreater(nameEnvironment);
        creater.getButton().drawButton(); //using same interface for button object
    }

    private static Creater getCreater (String nameEnvironment) {
        if (nameEnvironment.equals("win")) {
            return new CreaterWinButton();
        } else {
            return new CreaterHTMLButton();
        }
    }
}
